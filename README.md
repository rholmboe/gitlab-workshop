# Gitlab Workshop

## Starta

```bash
docker compose -p gitlab-workshop -f contrib/docker-compose.yml up -d
```

## Runner

Hämta token från: http://gitlab.localdev.me/admin/runners

```bash
docker compose -p gitlab-workshop -f contrib/docker-compose.yml exec runner \
    gitlab-runner register \
        --non-interactive \
        --url http://gitlab \
        --clone-url http://gitlab \
        --registration-token=<TOKEN> \
        --executor docker \
        --docker-image "docker:stable" \
        --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
        --docker-network-mode gitlab-network
```
