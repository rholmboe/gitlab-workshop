# docker build --rm -f contrib/dind.dockerfile -t docker:local-dind contrib

FROM docker:dind

RUN apk add --no-cache \
    python3 python3-dev py3-pip gcc git curl build-base \
    autoconf automake py3-cryptography linux-headers \
    musl-dev libffi-dev openssl-dev openssh && \
    python3 --version && \
    python3 -m venv /molecule-venv && \
    /molecule-venv/bin/python3 -m pip install --upgrade pip setuptools wheel && \
    /molecule-venv/bin/python3 -m pip install ansible ansible-lint "molecule[docker]" junit_xml
