# docker build --rm -f contrib/reno.dockerfile -t docker:local-reno contrib

FROM python:3.9

RUN python3 -m pip install 'reno[sphinx]'
